<div align="center">

  <!-- Keep the empty line between the div and this line on GitLab -->
  <br />
  <img src="https://gitlab.com/matrixpython/designs/-/raw/main/gitlab_logos/designs_and_logos_logo/designs_logo.svg"
         alt="MPWebsite"
         width="200"
  >
  </img>
  <br />
  <h1>Designs & Logos</h1>
  <br />

<h4>
  We are the Python Community on Matrix, a free and open network for secure,
  decentralized communication.
</h4>

<p>
  <a href="https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/blob/main/LICENSE_DOCS.md">
    <img alt="License"
         src="https://img.shields.io/badge/license-CC%20BY%204.0-blue"
    >
    </img>
  </a>
  <a href="https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/commits/main">
    <img alt="GitLab last commit"
         src="https://img.shields.io/gitlab/last-commit/matrixpython/matrixpython.gitlab.io"
    >
  </a>
</p>

<p>
  <a href="#contributing">Contributing</a> •
  <a href="#contact">Contact</a> •
  <a href="#license">License</a> •
  <a href="#trademarks">Trademarks</a>
</p>

<h5>In this repository we manage and share our designs and logos.</h5>

</div>


## Contributing

If you found a defect, or you want to get involved with the development of our
website, check our
[Contributing Wiki Page](https://matrixpython.gitlab.io/wiki/Help:Introduction/).
There you will find all necessary information to get started, how our triage
process works and the documentation.

[Bug Report or Feature Request →](https://gitlab.com/matrixpython/designs/-/issues/new)

## Contact

We have prepared an
[FAQ section](https://matrixpython.gitlab.io/docs/help/faq/), which might
answer your questions. If not, simply
[contact us](https://matrixpython.gitlab.io/contact/).

## License

All license are in the respective directories. Please make sure to check them
first before using the material.

If you don't find a license in a directory, the respective material is
[CC BY 4.0](https://gitlab.com/matrixpython/designs/-/blob/main/LICENSE_CC-BY-4.0.md)
licensed.

### License of This Repo's Logo

The license of this repository is in
https://gitlab.com/matrixpython/designs/-/blob/main/gitlab_logos/designs_and_logos_logo

## Trademarks

### Python

"Python" is a registered trademark of the PSF. The Python logos (in several
variants) are use trademarks of the PSF as well.

### Adobe® & Adobe® Illustrator®

Adobe, Adobe Illustrator are either registered trademarks or trademarks of
Adobe in the United States and/or other countries. <br /> THIS PRODUCT IS NOT
AUTHORIZED, ENDORSED OR SPONSORED BY ADOBE, PUBLISHER OF ADOBE ILLUSTRATOR.
