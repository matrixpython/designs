# Adobe® Illustrator® - Templates

This directory contains templates, we use in the design process.

## Swatches

| Swatch    | Type      | Description                                  | License                                                                               |
|-----------|-----------|----------------------------------------------|---------------------------------------------------------------------------------------|
| python.ai | Gradients | Contains the Python Logo Colors as gradients | [CC-BY-4.0](https://gitlab.com/matrixpython/designs/-/blob/main/LICENSE_CC-BY-4.0.md) |

Swatches can be opened directly or stored in
`%APPDATA%\Adobe\Adobe Illustrator <VERSION> Settings\en_US\x64\Swatches` as
user-defined swatches. Make sure to replace `<VERSION>` with your actual version 
of the installation of Adobe® Illustrator®, you use.