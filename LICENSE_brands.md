All brand icons are trademarks of their respective owners. The use of these
trademarks does not indicate endorsement of the trademark holder by the 
Python Community on Matrix or Michael Sasser, nor vice versa. 
**Please do not use brand logos for any purpose except
to represent the company, product, or service to which they refer.**
We changed the color of the icon to match our "corporate design".