# Internal Pages Logo of the Website Repository on GitLab

The following concerns the material located in this directory.

## How We Use The Material

We use the material as an gitlab project logo for our
[internal pages repository](https://gitlab.com/matrixpython/matrixpython.gitlab.io)
which is build togeter with our website.

## License

Copyright © 2023 Michael Sasser Info@MichaelSasser.org. Released
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license

### Icon

The globe icon is designed by Fonticons, Inc. (https://fontawesome.com)
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license
We changed the color of the icon to match our "corporate design"
and carved out a piece to fit in the id-badge (see below).
<br />
Link to the original:
- icon: https://fontawesome.com/icons/globe?s=solid
- license: https://github.com/FortAwesome/Font-Awesome/blob/6.x/LICENSE.txt
The id-badge icon is designed by Fonticons, Inc. (https://fontawesome.com)
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license
We changed the color of the icon to match our "corporate design".
<br />
Link to the original:
- icon: https://fontawesome.com/icons/id-badge?f=classic&s=solid
- license: https://github.com/FortAwesome/Font-Awesome/blob/6.x/LICENSE.txt