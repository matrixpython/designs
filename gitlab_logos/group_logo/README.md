# Python Room Icon

The following concerns the material located in this directory.

## How We Use The Material

We use the material as [gitlab group](https://gitlab.com/matrixpython) logo
on gitlab.

## License

All brand icons are trademarks of their respective owners. The use of these
trademarks does not indicate endorsement of the trademark holder by the 
Python Community on Matrix or Michael Sasser, nor vice versa. 
**Please do not use brand logos for any purpose except
to represent the company, product, or service to which they refer.**
We changed the color of the icon to match our "corporate design".


### Icon

All brand icons are trademarks of their respective owners. The use of these
trademarks does not indicate endorsement of the trademark holder by Font
Awesome, nor vice versa. **Please do not use brand logos for any purpose except
to represent the company, product, or service to which they refer.**
We changed the color of the icon to match our "corporate design".
<br />
Link to the original:
- icon: https://fontawesome.com/icons/python?s=brands
- license: https://github.com/FortAwesome/Font-Awesome/blob/6.x/LICENSE.txt