# Mjölnir Avatar

The following concerns the material located in this directory.

## How We Use The Material

We use the material as an avatar for our instance of the 
[mjolnir bot](https://github.com/matrix-org/mjolnir).

## License

Copyright © 2022 Michael Sasser Info@MichaelSasser.org. Released under the 
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.





