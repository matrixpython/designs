# Status Room Icon

The following concerns the material located in this directory.

## How We Use The Material

We use the material as an icon for our internal "Status" room on Matrix.

## License

Copyright © 2023 Michael Sasser Info@MichaelSasser.org. Released under the 
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.