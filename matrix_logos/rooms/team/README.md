# Internal Team Room Icon

The following concerns the material located in this directory.

## How We Use The Material

We use the material as an icon for our internal Team room and spaceon Matrix.

## License

Copyright © 2022 Michael Sasser Info@MichaelSasser.org. Released under the 
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.

### Icon

The icon is designed by Fonticons, Inc. (https://fontawesome.com) and is 
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) licensed.
We changed the color of the icon to match our "corporate design".

Link to the original:
- icon: https://fontawesome.com/icons/person-military-pointing?s=solid
- license: https://github.com/FortAwesome/Font-Awesome/blob/6.x/LICENSE.txt