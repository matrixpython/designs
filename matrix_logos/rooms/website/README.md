# Website Room Icon

The following concerns the material located in this directory.

## How We Use The Material

We use the material as an icon for our Website room on Matrix.

## License

Copyright © 2022 Michael Sasser Info@MichaelSasser.org. Released 
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license

### Icon

The icon is designed by Fonticons, Inc. (https://fontawesome.com)
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license
We changed the color of the icon to match our "corporate design".
<br />
Link to the original:
- icon: https://fontawesome.com/icons/globe?s=solid
- license: https://github.com/FortAwesome/Font-Awesome/blob/6.x/LICENSE.txt